# coding=utf-8

from flask.ext.script import Manager, Server

from karest.db_models import Channel, User
from services import app
from services.db_models import Report


manager = Manager(app)
manager.add_command("runserver", Server())


@manager.command
def init_db():
    print('Saving channel POSTMAN in BD...')
    postman = Channel()
    postman.id = 'POSTMAN2'
    # dt5nF0xBPNQ61dqQEofuFO5t4x9iu8l6
    postman.api_key_hash = '1b0ab66995210bf3a3fedaff864317f9fbaaf3ec8cad922d1780d247cfeb1de6'
    postman.requiredAppVersion = 'v1.0'
    postman.save()
    print('Done.')

    print('Saving user admin in BD...')
    admin = User()
    admin.id = 'admin'
    # admin123
    admin.password_hash = '240be518fabd2724ddb6f04eeb1da5967448d7e831c08c8fa822809f74c720a9'
    admin.properties['report_types'] = Report.VALID_TYPES
    admin.set_as_admin()
    admin.save()
    print('Done.')

    print('Saving user police operator in BD...')
    admin = User()
    admin.id = 'police'
    # p0l!ce
    admin.password_hash = 'e919fa74460a15f77cd62829b82eb04cd590c85b44019ca68a58ef285b43b076'
    admin.properties['report_types'] = ['CRIME']
    admin.role = 'OPERATOR'
    admin.save()
    print('Done.')


if __name__ == "__main__":
    manager.run()
