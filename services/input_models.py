from karest.fields import StringField, FloatField
from karest.base_models import InputModel
from services.db_models import Report


class CreateReport(InputModel):
    device_id = StringField(required=True, error_code='2')
    latitude = FloatField(required=True, error_code='3')
    longitude = FloatField(required=True, error_code='4')
    type = StringField(required=True, error_code='5', choices=Report.VALID_TYPES)
    description = StringField(error_code='6')


class AdminEditReport(InputModel):
    status = StringField(error_code = '3', choices=Report.VALID_STATUS)
    comments = StringField(error_code = '4')


class EditReport(InputModel):
    description = StringField(error_code='2')


class CreateSession(InputModel):
    user = StringField(required=True, error_code='2')
    password = StringField(required=True, error_code='3')