import datetime

from karest.base_models import OutputModel
from karest.db_models import User
from mongoengine import Document
from mongoengine.errors import DoesNotExist, ValidationError
from mongoengine.fields import  DateTimeField, EmailField, EmbeddedDocument, EmbeddedDocumentField, ListField, \
                                PointField, ReferenceField, StringField, URLField


class LogEntry(EmbeddedDocument, OutputModel):
    VALID_STATUS = ['RECEIVED', 'VALIDATED', 'INVALIDATED', 'RESOLVED']

    user = ReferenceField(User, required=True)
    date = DateTimeField(required=True, default=datetime.datetime.now)
    comments = StringField()
    status = StringField(required=True, choices=VALID_STATUS)


class Reporter(Document, OutputModel):
    VALID_STATUS = ['ACTIVE', 'BLOCKED']

    device_id = StringField(required=True, unique=True, primary_key=True)
    name = StringField()
    phone = StringField()
    email = EmailField()
    address = StringField()
    status = StringField(required=True, choices=VALID_STATUS, default='ACTIVE')

    @staticmethod
    def get_by_device_id(device_id):
        if device_id:
            try:
                return Reporter.objects.get(device_id=device_id)
            except (ValidationError, DoesNotExist):
                return None


class Report(Document, OutputModel):
    VALID_TYPES = ['CRIME', 'MEDICAL_ASSIST', 'WATER_LEAK', 'TRAFFIC_ACCIDENT', 'BROKEN_LIGHT', 'TRAFFIC_LIGHTS', 'TRASH', 'BROKEN_STREET', 'OTHER']
    VALID_STATUS = ['RECEIVED', 'VALIDATED', 'INVALIDATED', 'RESOLVED']

    reporter = ReferenceField(Reporter, required=True)
    type = StringField(required=True, choices=VALID_TYPES)
    location = PointField(required=True)
    description = StringField()
    photo_url = URLField()
    voice_note_url = URLField()
    date = DateTimeField(required=True, default=datetime.datetime.now)
    status = StringField(required=True, choices=VALID_STATUS, default='RECEIVED')
    history = ListField(EmbeddedDocumentField(LogEntry))

    @staticmethod
    def get_by_id(report_id):
        if report_id:
            try:
                return Report.objects.get(id=report_id)
            except (ValidationError, DoesNotExist):
                return None

    def change_status(self, new_status, user, comments=None):
        if new_status:
            self.status = new_status
            entry = LogEntry(user=user, status=new_status)
            if comments:
                entry.comments = comments
            self.history.append(entry)

    def set_location(self, longitude, latitude):
        self.location = {
            'type': 'Point',
            'coordinates': [latitude, longitude]
        }