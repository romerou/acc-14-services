from flask.ext.socketio import emit

from services import socketio


@socketio.on('subscribe', namespace='/test')
def on_connect(message):
    print('Connected')


@socketio.on('disconnect', namespace='/test')
def on_disconnect(message):
    print('Client disconnected')

