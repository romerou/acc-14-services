import logging
from logging.handlers import TimedRotatingFileHandler

from flask import Flask
from flask.ext.cors import CORS
from flask.ext.socketio import SocketIO
from mongoengine import connect

from karest import Api



# Application initialization
app = Flask(__name__)

# Configuration Variables
app.config['ALLOWED_AUDIO_EXTENSIONS'] = ['mp3', 'mp4']
app.config['ALLOWED_IMAGE_EXTENSIONS'] = ['png', 'jpg', 'jpeg']
#app.config['LOG_FILE_NAME'] = '/home/ubuntu/restaurants-app-plataforma/app.log'
app.config['LOG_LEVEL'] = logging.DEBUG
app.config['MONGODB_HOST'] = 'mongodb://localhost:27017/chacao'
app.config['MONGODB_NAME'] = 'chacao'
#app.config['REDIS_SERVER_HOST'] = 'localhost'
#app.config['REDIS_SERVER_PORT'] = '6379'
app.config['S3_ACCESS_KEY'] = 'AKIAIOOSX7VQKEY24FPQ'
app.config['S3_SECRET_KEY'] = '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'
app.config['S3_BUCKET'] = 'app-challenge-chacao'

# Logging configuration
formatter = logging.Formatter('%(asctime)s - %(thread)d - %(levelname)s - %(message)s')
file_name = app.config['LOG_FILE_NAME'] if app.config.has_key('LOG_FILE_NAME') else 'app.log'
fileHandler = TimedRotatingFileHandler(file_name, when='D')
fileHandler.setLevel(app.config['LOG_LEVEL'])
fileHandler.setFormatter(formatter)
app.logger.addHandler(fileHandler)
app.debug = app.config['LOG_LEVEL'] in [logging.DEBUG, logging.INFO]

# MongoDB Initialization
connect(app.config['MONGODB_NAME'], host=app.config['MONGODB_HOST'])

# Redis connection initialization
#pool = redis.ConnectionPool(app.config['REDIS_SERVER_HOST'], port=app.config['REDIS_SERVER_PORT'], db=0)
#redis = redis.Redis(connection_pool=pool)

# CORS Compatibility
CORS(app)

# karest framework
api = Api(app)

# SocketIO
socketio = SocketIO(app)

from services import resources