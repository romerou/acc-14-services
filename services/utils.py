from services import app


def allowed_image_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1] in app.config['ALLOWED_IMAGE_EXTENSIONS']

def allowed_audio_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1] in app.config['ALLOWED_AUDIO_EXTENSIONS']