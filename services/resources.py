import tinys3
from flask import jsonify, request
from flask.ext.socketio import disconnect, join_room, leave_room, send

from services.db_models import Report, Reporter
from input_models import AdminEditReport, CreateReport, CreateSession, EditReport
from karest.db_models import Session, User
from karest.errors import APIError
from karest.utils import file_ext, to_datetime
from services import api, app, socketio
from utils import allowed_audio_file, allowed_image_file


@app.route('/session', methods=['POST'])
@api.service_logger
@api.validate_json(CreateSession)
@api.api_key_auth
def create_session(input):
    user = User.get_by_id(input.user)
    if not user or not user.is_active():
        raise APIError('Invalid user', 400, '4')
    if not user.validate_password(input.password):
        raise APIError('Invalid password', 400, '5')

    session = Session()
    session.set_user(user)
    session.generate_cookie_id()
    session.save()

    # TODO ignore parameter for output models
    session.user.password_hash = None
    return session.to_api_response()


@app.route('/session', methods=['DELETE'])
@api.service_logger
@api.credentials_auth()
def delete_session(session):
    session.expire()
    session.save()

    response = {
        'result':'OK'
    }
    return jsonify(response)


@app.route('/report', methods=['POST'])
@api.service_logger
@api.api_key_auth
@api.validate_json(CreateReport)
def add_report(input):
    report = Report(type=input.type)
    report.set_location(input.longitude, input.latitude)

    reporter = Reporter.get_by_device_id(input.device_id)
    if not reporter:
        reporter = Reporter(device_id=input.device_id)
        reporter.save()
    report.reporter = reporter

    if input.description:
        report.description = input.description

    report.save()

    socketio.send(report.to_dict(), json=True, namespace='/reports', room=report.type)
    return report.to_api_response()


@app.route('/report/<report_id>', methods=['PUT'])
@api.service_logger
@api.api_key_auth
@api.validate_json(EditReport)
def edit_report(input, report_id):
    report = Report.get_by_id(report_id)
    if not report:
        raise APIError('Invalid Report Id', 404)

    if input.description:
        report.description = input.description

    report.save()

    socketio.send(report.to_dict(), json=True, namespace='/reports', room=report.type)
    return report.to_api_response()


@app.route('/report/<report_id>/voicenote', methods=['POST'])
@api.service_logger
@api.api_key_auth
def add_report_voice_note(report_id):
    report = Report.get_by_id(report_id)
    if not report:
        raise APIError('Invalid Report Id', 404)

    try:
        file = request.files['voice_note']
        if not file or not allowed_audio_file(file.filename):
            raise APIError("Invalid audio format: " + file_ext(file.filename), 400, 1)
    except Exception:
        raise APIError("Invalid file", 400, 1)

    s3_conn = tinys3.Connection(app.config['S3_ACCESS_KEY'], app.config['S3_SECRET_KEY'], endpoint='s3-us-west-2.amazonaws.com')
    filename = report_id + '_voice_note' + file_ext(file.filename)
    res = s3_conn.upload(filename, file, app.config['S3_BUCKET'])
    if res.status_code != 200:
        raise APIError(res.reason, 500)
    report.voice_note_url = res.url

    report.save()

    socketio.send(report.to_dict(), json=True, namespace='/reports', room=report.type)
    return report.to_api_response()


@app.route('/report/<report_id>/photo', methods=['POST'])
@api.service_logger
@api.api_key_auth
def add_report_photo(report_id):
    report = Report.get_by_id(report_id)
    if not report:
        raise APIError('Invalid Report Id', 404)

    try:
        file = request.files['image']
        if not file or not allowed_image_file(file.filename):
            raise APIError("Invalid image format", 400, 1)
    except Exception:
        raise APIError("Invalid file", 400, 1)

    s3_conn = tinys3.Connection(app.config['S3_ACCESS_KEY'], app.config['S3_SECRET_KEY'], endpoint='s3-us-west-2.amazonaws.com')
    filename = report_id + '_photo' + file_ext(file.filename)
    res = s3_conn.upload(filename, file, app.config['S3_BUCKET'])
    if res.status_code != 200:
        raise APIError(res.reason, 500)
    report.photo_url = res.url

    report.save()

    socketio.send(report.to_dict(), json=True, namespace='/reports', room=report.type)
    return report.to_api_response()


@app.route('/admin/report/<report_id>', methods=['PUT'])
@api.service_logger
@api.credentials_auth(valid_roles=['ADMIN'])
@api.validate_json(AdminEditReport)
def edit_report_admin(input, session, report_id):
    report = Report.get_by_id(report_id)
    if not report:
        raise APIError('Invalid Report Id', 404)

    if input.status:
        report.change_status(input.status, session.user, input.comments)

    report.save()

    socketio.send(report.to_dict(), json=True, namespace='/reports', room=report.type)
    return report.to_api_response()


@app.route('/reports', methods=['GET'])
@api.service_logger
@api.credentials_auth(valid_roles=['ADMIN', 'OPERATOR'])
@api.paginated_service
def get_reports(session):
    filters = {}

    device_id = request.args.get('device_id')
    if device_id:
        reporter = Reporter.get_by_device_id(device_id)
        filters['reporter'] = reporter

    types = request.args.getlist('type')
    if types and len(types) != 0:
        filters['type__in'] = types

    status = request.args.getlist('status')
    if status and len(status) != 0:
        filters['status__in'] = status

    from_date_str = request.args.get('from_date')
    if from_date_str:
        from_date = to_datetime(from_date_str)
        filters['date__gte'] = from_date

    to_date_str = request.args.get('to_date')
    if to_date_str:
        to_date = to_datetime(to_date_str)
        filter['date__lte'] = to_date

    return Report.objects(**filters)


@app.route('/report/<report_id>', methods=['GET'])
@api.service_logger
@api.credentials_auth(valid_roles=['ADMIN', 'OPERATOR'])
def get_report(session, report_id):
    report = Report.get_by_id(report_id)
    if not report:
        raise APIError('Invalid Report Id', 404)

    return report.to_api_response()


@socketio.on('connect', namespace='/reports')
def on_connect():
    pass


@socketio.on('register', namespace='/reports')
def on_register(json):
    app.logger.info('Registering socket')
    app.logger.debug('JSON: ' + str(json))
    if not json['cookie_id']:
        response = {'code': '0', 'message': 'Invalid Cookie-Id'}
        send(response)
        app.logger.info('Disconnecting from socket: ' + str(response))
        disconnect()
    cookie_id = json['cookie_id']

    session = Session.get_by_cookie(cookie_id)
    if not session or not session.is_active():
        response = {'code': '0', 'message': 'Invalid Cookie-Id'}
        send(response)
        app.logger.info('Disconnecting from socket: ' + str(response))
        disconnect()

    user = session.user
    if not user.role in ['ADMIN', 'OPERATOR']:
        response = {'code': '1', 'message': 'User must have role ADMIN or OPERATOR'}
        send(response)
        app.logger.info('Disconnecting from socket: ' + str(response))
        disconnect()


    report_types = session.user.properties['report_types']
    if report_types and isinstance(report_types, list):
        for report_type in report_types:
            app.logger.info('Joining room: ' + report_type)
            join_room(report_type)


@socketio.on('disconnect', namespace='/reports')
def on_disconnect():
    pass


@socketio.on('close', namespace='/reports')
def on_close(json):
    if not json['cookie_id']:
        response = {'code': '0', 'message': 'Invalid Cookie-Id'}
        send(response)
        disconnect()
    cookie_id = json['cookie_id']

    session = Session.get_by_cookie(cookie_id)
    if not session or not session.is_active():
        response = {'code': '0', 'message': 'Invalid Cookie-Id'}
        send(response)
        disconnect()

    user = session.user
    if not user.role in ['ADMIN', 'OPERATOR']:
        response = {'code': '1', 'message': 'User must have role ADMIN or OPERATOR'}
        send(response)
        disconnect()

    report_types = session.user.properties['report_types']
    if report_types and isinstance(report_types, list):
        for report_type in report_types:
            leave_room(report_type)

    disconnect()


@app.route('/swagger', methods=['GET'])
def get_swagger():
    swagger_file = open('swagger.json', 'r')
    swagger_str = swagger_file.read()
    swagger_file.close()
    return swagger_str
