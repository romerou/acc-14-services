import datetime
import hashlib
import os

from base_models import OutputModel
from mongoengine import DictField, Document, EmailField, IntField, StringField, DateTimeField, ReferenceField
from mongoengine.errors import ValidationError, DoesNotExist


class User(Document, OutputModel):
    _VALID_STATUS = ['ACTIVE', 'INACTIVE']
    _VALID_ROLES = ['ADMIN', 'OPERATOR', 'CLIENT']
    _VALID_GENDERS = ['MALE', 'FEMALE']

    id = StringField(required=True, unique=True, primary_key=True)
    # SHA256
    password_hash = StringField()
    role = StringField(required=True, choices=_VALID_ROLES, default='CLIENT')
    device_id = StringField()
    first_name = StringField()
    last_name = StringField()
    email = EmailField()
    age = IntField(min_value=1)
    gender = StringField(choices=_VALID_GENDERS)
    status = StringField(required=True, choices=_VALID_STATUS, default='ACTIVE')
    properties = DictField()

    @staticmethod
    def get_by_id(user_id):
        if user_id:
            try:
                return User.objects.get(id=user_id)
            except (ValidationError, DoesNotExist):
                return None

    @staticmethod
    def is_valid_status(status):
        return status in User._VALID_STATUS

    @staticmethod
    def is_valid_role(role):
        return role in User._VALID_ROLES
    
    def validate_password(self, password):
        if password:
            password_hash = hashlib.sha256(password).hexdigest()
            return password_hash == self.password_hash

    def set_as_admin(self):
        self.role = 'ADMIN'

    def is_admin(self):
        return self.role == 'ADMIN'

    def set_as_client(self):
        self.role = 'CLIENT'

    def is_client(self):
        return self.role == 'CLIENT'

    def is_active(self):
        return self.status == 'ACTIVE'

    def set_password(self, password):
        if password:
            self.password_hash = hashlib.sha256(password).hexdigest()


class Session(Document, OutputModel):
    _VALID_STATUS = ['ACTIVE', 'EXPIRED']

    cookieId = StringField(required=True, unique=True, primary_key=True)
    user = ReferenceField(User, required=True)
    date = DateTimeField(required=True, default=datetime.datetime.now)
    status = StringField(required=True, choices=_VALID_STATUS, default='ACTIVE')
    properties = DictField()

    @staticmethod
    def get_by_cookie(cookie_id):
        if cookie_id:
            try:
                return Session.objects.get(cookieId=cookie_id)
            except (ValidationError, DoesNotExist):
                return None

    @staticmethod
    def get_user_active_sessions(user):
        if user:
            return Session.objects(user=user, status='ACTIVE')

    def generate_cookie_id(self):
        self.cookieId = os.urandom(32).encode('hex')

    def is_active(self):
        return self.status == 'ACTIVE'

    def expire(self):
        self.status = 'EXPIRED'

    def set_user(self, user):
        if user:
            current_sessions = Session.get_user_active_sessions(user)
            for session in current_sessions:
                session.expire()
                session.save()
            self.status = 'ACTIVE'
            self.user = user


class Channel(Document, OutputModel):
    name = StringField(required=True, unique=True, primary_key=True)
    # SHA256
    api_key_hash = StringField(required=True, unique=True)
    required_app_version = StringField()
    properties = DictField()

    @staticmethod
    def get_by_api_key(api_key):
        if api_key:
            api_key_hash = hashlib.sha256(api_key).hexdigest()
            return Channel.objects(api_key_hash=api_key_hash)