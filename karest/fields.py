import re
from utils import is_valid_url, is_valid_email


class ValidationResult:

    def __init__(self, is_valid=False, code=None, message=None):
        self.is_valid = is_valid
        self.code = code
        self.message = message


class BaseField(object):

    def __init__(self, instance=basestring, required=False, choices=None, error_code=None, error_message=None):
        self.instance = instance
        self.required = required
        self.choices = choices if isinstance(choices, list) else None
        self.error_code = error_code
        self.error_message = error_message

    def validate(self, value):
        if self.required and not value:
            return ValidationResult(False, '0', 'value null or empty')

        if value:
            if not isinstance(value, self.instance):
                return ValidationResult(False, '1', 'Not a ' + str(self.instance) + ' value')
        else:
            return ValidationResult(True)


class StringField(BaseField):

    def __init__(self, regex=None, max_length=None, min_length=None, **kwargs):
        super(StringField, self).__init__(instance=basestring, **kwargs)
        self.instance = basestring
        self.regex = re.compile(regex) if regex else None
        self.max_length = max_length
        self.min_length = min_length

    def validate(self, value):
        base_validation = super(StringField, self).validate(value)
        if base_validation:
            return base_validation

        if self.choices:
            if value not in self.choices:
                return ValidationResult(False, '2', 'String value is not a valid choice (' + str(self.choices) + ')')

        if self.max_length is not None and len(value) > self.max_length:
            return ValidationResult(False, '3', 'String value is too long')

        if self.min_length is not None and len(value) < self.min_length:
            return ValidationResult(False, '4', 'String value is too short')

        if self.regex is not None and self.regex.match(value) is None:
            return ValidationResult(False, '5', 'String value did not match validation regex')

        return ValidationResult(True)


class URLField(BaseField):

    def __init__(self, **kwargs):
        super(URLField, self).__init__(**kwargs)
        self.instance = basestring

    def validate(self, value):
        base_validation = super(URLField, self).validate(value)
        if base_validation:
            return base_validation

        if not is_valid_url(value):
            return ValidationResult(False, '2', 'Invalid URL: ' + value)

        return ValidationResult(True)


class EmailField(BaseField):

    def __init__(self, **kwargs):
        super(EmailField, self).__init__(**kwargs)
        self.instance = basestring

    def validate(self, value):
        base_validation = super(EmailField, self).validate(value)
        if base_validation:
            return base_validation

        if not is_valid_email(value):
            return ValidationResult(False, '2', 'Invalid Mail-address: ' + value)

        return ValidationResult(True)


class IntField(BaseField):

    def __init__(self, min_value=None, max_value=None, **kwargs):
        super(IntField, self).__init__(**kwargs)
        self.instance = int
        self.min_value = min_value
        self.max_value = max_value

    def validate(self, value):
        base_validation = super(IntField, self).validate(value)
        if base_validation:
            return base_validation

        if self.min_value is not None and value < self.min_value:
            return ValidationResult(False, '2', 'Integer value is too small')

        if self.max_value is not None and value > self.max_value:
            return ValidationResult(False, '3', 'Integer value is too large')

        return ValidationResult(True)


class FloatField(BaseField):

    def __init__(self, min_value=None, max_value=None, **kwargs):
        super(FloatField, self).__init__(**kwargs)
        self.instance = float
        self.min_value = min_value
        self.max_value = max_value

    def validate(self, value):
        base_validation = super(FloatField, self).validate(value)
        if base_validation:
            return base_validation

        if self.min_value is not None and value < self.min_value:
            return ValidationResult(False, '1', 'Float value is too small')

        if self.max_value is not None and value > self.max_value:
            return ValidationResult(False, '2', 'Float value is too large')

        return ValidationResult(True)


class BooleanField(BaseField):

    def __init__(self, **kwargs):
        super(BooleanField, self).__init__(**kwargs)
        self.instance = bool

    def validate(self, value):
        base_validation = super(BooleanField, self).validate(value)
        if base_validation:
            return base_validation

        return ValidationResult(True)