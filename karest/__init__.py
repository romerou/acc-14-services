# -*- coding: utf-8 -*-
"""
    karest
    ~~~~~
    A microframework based on Flask.
    :copyright: (c) 2015 by Carlos Romero.
    :license: BSD, see LICENSE for more details.
"""

from api import Api


__version__ = '0.1'
__all__ =  ['Api']