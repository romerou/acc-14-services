import json

from bson import DBRef
from errors import APIError
from flask import jsonify
from mongoengine import Document


class InputModel(object):

    def validate(self):
        for key, value in self.__dict__.iteritems():
            field = self.__class__.__dict__.get(key)
            if isinstance(field, tuple):
               field = field[0]
            validation_result = field.validate(value)
            if not validation_result.is_valid:
                message = 'Input parameter \'' + key + '\': ' + validation_result.message
                message = value.error_message + ' - ' + message if field.error_message else message
                raise APIError(message, 400, field.error_code)

    @classmethod
    def from_json(cls, json_str):
        json_obj = json.loads(json_str)
        model_obj = cls()
        for key in cls.__dict__.iterkeys():
            if not key.startswith('__'):
                model_obj.__setattr__(key, json_obj.get(key))
        return model_obj


class OutputModel(object):

    def to_api_response(self):
        return jsonify(self.to_dict())

    def to_dict(self):
        resp_dict = {}
        if isinstance(self, Document):
            for key, value in self._data.iteritems():
                if value:
                    if isinstance(value, OutputModel):
                        resp_dict[key] = value.to_dict()
                    elif isinstance(value, DBRef):
                        field = self.__class__.__dict__.get(key)
                        obj = field.document_type_obj.objects.get(pk=str(value.id))
                        resp_dict[key] = obj.to_dict()
                    else:
                        resp_dict[key] = OutputModel._value_to_response_value(value)
        return resp_dict

    @staticmethod
    def _value_to_response_value(value):
        if not value:
            return None
        if isinstance(value, list):
            return [OutputModel._value_to_response_value(item) for item in value]
        elif isinstance(value, int) or isinstance(value, float) or isinstance(value, bool) or isinstance(value, dict):
            return value
        elif isinstance(value, OutputModel):
            return value.to_dict()
        elif isinstance(value, basestring):
            return value.encode('utf-8').strip()
        else:
            return str(value)