class APIError(Exception):

    def __init__(self, error_message, status_code, error_code=None):
        Exception.__init__(self)
        self.error_code = error_code
        self.error_message = error_message
        self.status_code = status_code

    def to_dict(self):
        rv = {
            "status": self.status_code,
            "message": self.error_message
        }
        if self.error_code is not None:
            rv['code'] = self.error_code
        return rv