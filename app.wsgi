#!/usr/bin/python
import sys, logging, site

# Add the site-packages of the chosen virtualenv to work with
site.addsitedir('/home/ubuntu/restaurants-app-plataforma/venv/local/lib/python2.7/site-packages')

logging.basicConfig(stream=sys.stderr)
sys.path.insert(0,"/home/ubuntu/restaurants-app-plataforma")

from rest import app as application
application.secret_key = 'Add your secret key'
